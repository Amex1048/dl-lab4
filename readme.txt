AES encryption-decryption library:
aes128_encrypt.rs contains encrypt function that encrypts message with 128bit key
aes128_decrypt.rs contains decrypt function that decrypts message with 128bit key

main.rs contains 2 examples from AES documentation
https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.197.pdf

Building:
rustc 1.59(or greater) compiler + relevant cargo version
Commands:
    cargo build --release
    cargo run --release